# To be honest, most of these are just the runtime versions of the build deps
# I have no idea which ones are actually required
btrfs-progs: pkg.installed
iptables: pkg.installed
libapparmor1: pkg.installed
libassuan0: pkg.installed
libc6: pkg.installed
libdevmapper1.02.1: pkg.installed
libglib2.0-0: pkg.installed
libgpg-error0: pkg.installed
libgpgme11: pkg.installed
libostree-1-1: pkg.installed
libprotobuf-c1: pkg.installed
libprotobuf17: pkg.installed
libseccomp2: pkg.installed
libselinux1: pkg.installed
libsystemd0: pkg.installed


# Config files
/etc/containers:
  file.directory

/etc/containers/registries.conf:
  file.managed:
    - source: salt://_artifacts/conf/registries.conf
    - require:
      - file: /etc/containers

/etc/containers/policy.json:
  file.managed:
    - source: salt://_artifacts/conf/policy.json
    - require:
      - file: /etc/containers

/etc/cni/net.d:
  file.directory:
    - makedirs: true

# See also: https://github.com/containers/libpod/blob/master/cni/README.md
# /etc/cni/net.d/99-loopback.conf:
#   file.managed:
#     - source: salt://_artifacts/conf/99-loopback.conf
#     - require:
#       - file: /etc/cni/net.d


# podman binaries
{% if grains['osfinger'] == 'Debian-testing' %}
containers-kubric:
  pkgrepo.managed:
    - name: 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_Testing/ /'
    - key_url: https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Debian_Testing/Release.key
{% elif grains['osfinger'] == 'Debian-10' %}
containers-kubric:
  pkgrepo.managed:
    - name: 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /'
    - key_url: https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/Debian_10/Release.key
{% endif %}

podman: pkg.installed
buildah: pkg.installed
